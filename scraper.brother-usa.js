var fs = require('fs');
var path = require('path');
var cheerio = require('cheerio');
var request = require('request');
var pretty = require('pretty');

var sourceURL = 'http://www.brother-usa.com/Printer/ModelDetail/1/hll5100dn';
var destFile = path.resolve(__dirname, 'hl-l5100dn.html');
var tmplFile = path.resolve(__dirname, 'template.html');
var prettify = true;

// load template
$$ = cheerio.load(fs.readFileSync(tmplFile, 'utf8'));
var $template = $$('html');

var featuresHTML = '';
var overviewHTML = '';
var specsHTML = '';

request({ method: 'GET', url: sourceURL + '/overview' }, function(err, response, body) {

  if (err) return console.error(err);

  $ = cheerio.load(body);

  // Build overview section
  var $ov = $('#ctl00_ContentPlaceHolder1_modDetail_divOverview');
  if($ov) {
    overviewHTML += $('<h2/>').html($ov.find('.TabContentHeader span').html()).attr('style', 'text-align:center');
    overviewHTML += $('<hr>');
    overviewHTML += $('<p/>').html($ov.find('#ctl00_ContentPlaceHolder1_modDetail_pLongDescription').html());
  }

  // Build features section
  var $feat = $('#ctl00_ContentPlaceHolder1_modDetail_divFeatures');
  if($feat.length) {
    featuresHTML += $('<h2/>').attr('style', 'text-align:center').html($feat.find('.TabContentHeader span').html());
    featuresHTML += $('<hr>');
    $feat.find('span.NoColor_Bold_None').replaceWith(function(){ return $('<strong/>').html($(this).html()); });
    $feat.find('span').replaceWith(function(){ return $('<em/>').html($(this).html()); });
    $feat.find('a').replaceWith(function(){ return $('<em/>').html($(this).html()); });
    featuresHTML += $('<ul/>').html($feat.find('.FeaturesList').html());
  }

  // Append scrapes to template
  $template.find('.product_overview').html(overviewHTML);
  $template.find('.product_features').html(featuresHTML);

  scrapeSpecs();

});

function scrapeSpecs() {

  request({ method: 'GET', url: sourceURL + '/spec' }, function(err, response, body) {

    if (err) return console.error(err);
    $ = cheerio.load(body);

    var $spec = $('#ctl00_ContentPlaceHolder1_modDetail_divSpecs');

    specsHTML += $('<h2/>').html('Technical Specifications').attr('style', 'text-align:center');
    specsHTML += $('<hr>');

    if($spec) {
      $spec.find('.TabContentHeader').each(function () {
        var $heading = $(this).find('.DarkBlue_Bold_100');
        var $table = $(this).next('.AccSpecTable');

        $table.find('td:nth-child(1)').attr('width', '40%');
        $table.find('td:nth-child(3)').remove();
        $table.find('.NoColor_Bold_67').replaceWith(function(){ return $('<strong/>').html($(this).html()); });
        $table.find('[class]').removeAttr('class');
        $table.find('[id]').removeAttr('id');
        $table.find('[style]').removeAttr('style');
        $table.find('[align]').removeAttr('align');
        $table.find('a').replaceWith(function(){ return $('<em/>').html($(this).html()); });

        specsHTML += $('<h3/>').html($heading.html());
        specsHTML += $('<table/>').html($table.html()).attr('width', '100%').attr('style', 'font-size:12px');

      });

    }



    $template.find('.product_techspecs').html(specsHTML);

    // Write to file

    var newHTML = prettify ? pretty($template.html()) : $template.html();

    fs.writeFile(destFile, newHTML, 'utf-8');

  });

}
