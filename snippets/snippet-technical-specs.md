### General

*   **Model** MFC-9340CDW
*   **Number of Functions** 5 in 1
*   **Engine** Colour LED
*   **Functions** Laser Printer, Laser Fax, Flatbed Colour Scanner, Flatbed Digital Laser Copier, PC Fax
*   **Secure Function Lock** Up to 25 users
*   **Backup For Clock** Up to 60 hours
*   **LDAP** NA
*   **Standard Memory** 256MB
*   **ISIS** NA

### Display

*   **LCD Display** 9.3cm TFT Colour Touchscreen LCD

### Telephone

*   **Telephone Handset** NA
*   **Speaker Phone** NA

### Printer

*   **Emulation** PCL6, BR-Script3
*   **Print Speed** Up to 22 ppm Mono  
    Up to 22 ppm Colour
*   **CPU Speed** 333MHz
*   **Printer Resolution** 600 x 600 dpi, 2400 dpi (600 x 2400) quality
*   **Printer Compatibility** Windows 8 (32&64bit), Windows 7 (32&64bit), Vista (32&64bit), XP Home Edition, XP Professional (32&64bit), Windows Server 2012, Windows Server 2008 (32&64bit), Windows Server 2008 R2, Windows Server 2003 (64bit), (All server Operating Systems are network print only)  
    Mac OS X 10.6.8 or greater  
    Linux CUPS, LPD/LPRng (x86/x64 environment)
*   **Print Speed (4x6)** NA
*   **Duplex Print** Built-in @ Up to 7/7 ipm (Mono/Colour, based on ISO/IEC 24734)
*   **Disc Print** NA
*   **First Print Out Time** Less than 16sec Mono / Less than 16sec Colour (from ready mode)
*   **Warm Up Time** From Sleep Mode : less than 24 secs  
    From Power ON : less than 25 secs
*   **Printer Driver Functions** Windows: Poster, N in 1, Header-Footer, Watermark, Toner Save Mode, Duplex, Secure Print, Print Profiles  
    Mac: Colour Enhancement, Duplex, Secure Print
*   **Resident Fonts** PCL6 : 66 scalable fonts, 12 bitmap fonts, 13 bar codes  
    BR-Script 3 (Post Script 3 language emulation) : 66 scalable fonts
*   **Secure Print** Windows/ Mac
*   **Borderless Printing** NA

### Paper Handling

*   **Standard Paper Capacity** Up to 250 Sheets
*   **Photo Tray Capacity** NA
*   **Multi-Purpose Tray Capacity** NA
*   **Manual Feed Slot** 1 Sheet
*   **Output Paper Capacity** Up to 100 sheets (Front), 1 Sheet (Rear)
*   **ADF** Up to 35 sheets
*   **Media Weights Paper Tray** 60 - 105 gsm
*   **Media Weights MP Tray** NA
*   **Media Weights Manual Feed Slot** 60 - 163 gsm
*   **Media Weights PhotoPaper Tray** NA
*   **Media Weights ADF** 64 - 90 gsm
*   **Media Weights Duplex** 60 - 105 gsm
*   **Media Weights Paper Tray (Option)** NA
*   **Media Size Paper Tray** A4, Letter, B5(JIS), A5, A5(Long Edge), A6, Exective, Legal, Folio
*   **Media Size MP Tray** NA
*   **Media Size Manual Feed Slot** 76.2 to 216mm(W), 116 to 355 mm(L)
*   **Media Size ADF** Width 147.3 to 215.9 mm, Length 147.3 to 356.0 mm
*   **Media Size Duplex** A4
*   **Media Size Paper Tray (Option)** NA
*   **Media Size Scanner Glass** Up to A4/LTR
*   **Media Types Paper Tray** Plain Paper, Thin Paper, Recycled Paper
*   **Media Types MP Tray** see manual feed slot
*   **Media Types Manual Feed Slot** Plain Paper, Thin Paper, Thick Paper, Thicker Paper, Recycled Paper, Bond Paper, Label, Envelope, Env Thin, Env Thick, Glossy Paper
*   **Media Types PhotoPaper Tray** NA
*   **Media Types ADF** Plain, Recycled Paper
*   **Media Types Duplex** Plain Paper, Thin Paper, Recycled Paper
*   **Media Types Paper Tray (Option)** NA

### Digital Copier

*   **Copier Speed** Up to 22 cpm Mono  
    Up to 22 cpm Colour
*   **Copier Resolution** Maximum 600 x 600 dpi
*   **Copier Enlargement-Reduction Ratio** 25 - 400 in 1% increments
*   **Copier N-in-1** 2 in 1 & 4 in 1 (A4/LTR only)
*   **Copier Poster** NA
*   **Copier Multi Sort** Yes
*   **Copier Multi Stack** Up to 99 copies
*   **First Copy Out Time** Less than 19sec Mono / Less than 22sec Colour (from ready mode)


### Fax

*   **Modem Speed** 33,600bps (Fax)
*   **Transmission Speed** Approx. 2 sec. (ITU-T Test Chart No1, Std resolution, JBIG)
*   **Fax Receive Stamp** Yes
*   **Duplex Fax** Send / Receive
*   **Display Fax** Receive Only
*   **Colour Faxing** NA
*   **ITU-T Group compatibility** Super G3
*   **Telstra Duet Compatible** Yes
*   **Memory Backup** Yes
*   **Memory Security** NA
*   **Automatic Redial** Yes
*   **One Touch Dials** NA
*   **Speed Dials** 200 locations
*   **Group Dials** Up to 20 groups
*   **Station ID** 20 digits / 20 characters
*   **Caller ID** NA
*   **Fax Grey Scale** 256 levels
*   **Out of Paper Reception** Up to 500 pages (ITU-T Test Chart)
*   **Broadcasting** Up to 250 locations
*   **External TAD** Socket Only
*   **Batch Transmission** Yes
*   **Automatic Reduction** Yes
*   **Fax Forwarding** To Fax number or (Email Address-(Download Firmware from Brother Solutions Centre)
*   **Speaker/Ring volume adjustment** 3 steps plus OFF
*   **Memory Transmission** Up to 500 pages (ITU-T Test Chart)

### Fax Features

*   **PC Fax** Send/Receive from Windows (Network / USB)  
    Send only from MAC / Linux
*   **Network PC FAX** Send/Receive from Windows, Send Only MAC/Linux (USB/LAN)
*   **Internet Fax** Yes - T37 protocol (Download Firmware from Brother Solutions Centre)
*   **Fax to Server** Yes (Download Firmware from Brother Solutions Centre)

### Message Center

*   **Internal TAD** NA
*   **In Coming Message Time** NA
*   **User Recording Time** NA
*   **Answering Machine** NA
*   **Out Going Message Time** NA

### Digital Scanner

*   **Scanner Colour/Mono** Colour
*   **Scanner Colour Depth** 48 bit input / 24 bit output colour
*   **Scanner Type** Flatbed Digital
*   **Scanning Technology** Dual CIS
*   **Scan Speed** Max 2.32 Sec Mono / 3.09 Sec Colour @A4_size at 300dpi
*   **Scanner Resolution (Optical)** From Glass: Up to 1200 x 2400 dpi  
    From ADF: Up to 1200 x 600 dpi
*   **Scanner Resolution (Interpolated)** Up to 19,200 x 19,200 dpi
*   **Duplex Scan** Yes
*   **Scanning Area** A4 (ADF/FB): 210/204 mm  
    LTR (ADF/FB): 210 mm
*   **Scanner Grey Scale** 256 levels
*   **Scan to Keys** Scan To: Email Client/ Email Server(Web Download)/ OCR/ Image/ File/ FTP/ USB/ Network
*   **Scanner Compatibility** Windows XP Home Edition, XP Professional, XP Professional x64 Edition, Vista, Windows 7, windows 8  
    Mac OS X 10.6.8 or greater  
    Linux Sane
*   **Twain Compliant** Windows: TWAIN or (WIA for XP/Vista/Windows7/Windows8)  
    Mac: TWAIN  
    Linux: SANE

### Photo Capture

*   **PictBridge** NA
*   **Digital-Media Type** USB memory drive
*   **Digital-Media Spec** USB memory drive up to 32Gb
*   **Image Format-Media Card** PDF version1.7, JPEG, Exif+JPEG, PRN(created by own printer driver)  
    TIFF(scanned by Brother model), XPS version 1.0
*   **Direct Print USB** Yes
*   **Removable Media Card/USB memory** Read & Write to USB memory drive

### Software

*   **Software Included** Windows: Scansoft Paperport 12SE and OCR, Status Monitor, Control Centre4, BRAdmin Light, Web Base Management, Network Remote Setup, Driver Deployment Wizard  
    Mac: Page Manager9, Status Monitor, Control Centre2, Remote Setup, PC Fax Send, BRAdmin (Light)

### Connectivity

*   **Interface** Hi-Speed USB2.0  
    10/100 BaseTX Ethernet  
    802.11 b/g/n Wireless
*   **Interface Parallel** NA
*   **Network Interface Standard** 10/100BASE-TX Ethernet and 802.11 b/g/n Wireless
*   **NFC** NA
*   **Wi-Fi Direct** Yes
*   **Airprint** Yes
*   **iPrintScan** Yes
*   **Web Connect** Scan To - Download and Print from Picasa, Flickr, Facebook, Google Drive, Evernote, Dropbox, Box, Skydrive
*   **Google Cloud Print** Yes

### Networking

*   **Wireless Interface** IEEE 802.11b/g/n (Infrastructure Mode/Adhoc mode)
*   **Wireless Security** WEP 64/128 bit, WPA-PSK (TKIP/AES), WPA2-PSK (AES)
*   **Network Protocols IPV4** ARP, RARP, BOOTP, DHCP, APIPA(Auto IP), WINS/NetBIOS name resolution, DNS Resolver, mDNS, LLMNR responder, LPR/LPD, Custom Raw Port/Port9100, POP3, SMTP Client, IPP/IPPS, FTP Client and Server, CIFS Client, TELNET Server, SNMPv1/v2c/v3, HTTP/HTTPS server, TFTP client and server, ICMP, Web Services (Print/Scan), SNTP Client
*   **Network Protocols IPV6** NDP, RA, DNS resolver, mDNS, LLMNR responder, LPR/LPD, Custom Raw Port/Port9100, IPP/IPPS, FTP Client and Server, CIFS Client, TELNET Server, SNMPv1/v2c/v3, HTTP/HTTPS server, TFTP client and server, SMTP Client, ICMPv6, SNTP Client, Web Service
*   **Network Management** BRAdmin Professional  
    Web Based Management  
    BRAdmin Light
*   **Network Scan** Yes
*   **Network Media** NA
*   **Email Alerts** E-mail Notification / Reports

### Options

*   **Optional Paper Tray** NA
*   **Optional Memory** NA
*   **Optional Paper** NA

### Consumable

*   **Consumables** Black Toner (TN-251BK) Up to 2,500 pages,  
    Colour Toner (TN-251C/ TN-251M/ TN-251Y) Up to 1,400 pages in accordance with ISO/IEC19798  
    Colour High Yield Toner (TN-255C/ TN-255M/ TN-255Y) Up to 2,200 pages in accordance with ISO/IEC19798  
    Drum (DR-251CL) Up to 15,000 pages (1 page per job)  
    Belt Unit (BU-220CL) Up to 50,000 pages (by continuous printing)  
    Waste Toner Box (WT-220CL) 50,000 pages
*   **Inbox Consumables** Black/Cyan/Magenta/Yellow Toner Cartridges Up to 1,000 pages in accordance with ISO/IEC19798.  
    Drum (DR-251CL) Up to 15,000 A4 pages (1 page per job)  
    Belt Unit (BU-220CL) Up to 50,000 pages (by continuous printing)  
    Waste Toner Box (WT-220CL) 50,000 pages


### Weight and Measure

*   **Size** 410(W) x 483(D) x 410(H)mm
*   **Weight** 18.8kg

### Environment

*   **Energy Star Compliant** Yes
*   **Power Consumption** Printing: Average 375 W  
    Ready: Average 70 W  
    Sleep: Average 7.5 W  
    Deep Sleep: Average 0.05 W
*   **Noise Level** Sound Pressure Level: (Printing) Less than 53 dBA, (Ready) Less than 33 dBA  
    Sound Power Level: (Printing) Less than 6.42 B, (Ready) Less than 4.44 B
*   **Sleep Mode** (00-50min)
*   **TEC Value** 1.4 kWh/Week
*   **Humidity** Operating: 20 - 80% (without condensation)  
    Storage: 10 - 90% (without condensation)
*   **Temperature** Operating: 10 - 32.5°C  
    Storage: 0 - 40°C

### Others

*   **Warranty** 12 Months Return To Base  
    [<u>Warranty Information</u>](http://brother.com.au/service-and-support/standard-warranty.html)
*   **Recommended Monthly Print Volume** 300 to 1,500 pages
*   **Carton Contents** MFC-9340CDW, CD-ROM (including User's Guide), Quick Setup Guide, Power Cord, Telephone Line Cord, Telephone Line Cord Adapter, Starter Black/ Cyan/ Magenta/ Yellow Toner Cartridges Up to 1,000 pages in accordance with ISO/IEC19798.  
    Drum (DR-251CL) Up to 15,000 A4 pages (1 page per job)  
    Belt Unit (BU-220CL) Up to 50,000 pages (by continuous printing)  
    Waste Toner Box (WT-220CL) 50,000 pages
